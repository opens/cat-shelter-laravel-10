<?php

namespace App\Http\Controllers;

use App\Http\Resources\BranchResource;
use App\Models\Branch;
use App\Http\Requests\BranchRequest;

class BranchController extends Controller
{
    public function index()
    {
        $branches = Branch::with('cat')->get();
        return BranchResource::collection($branches);
    }

    public function store(BranchRequest $request)
    {
        $branch = Branch::create($request->validated());
        return new BranchResource($branch);
    }

    public function show($id)
    {
        $branch = Branch::findOrFail($id);
        return new BranchResource($branch);
    }

    public function update(BranchRequest $request, $id)
    {
        $branch = Branch::findOrFail($id);
        $branch->update($request->validated());
        return new BranchResource($branch);
    }

    public function destroy($id)
    {
        $branch = Branch::findOrFail($id);
        $branch->delete();
        return response()->json(null, 204);
    }
}
