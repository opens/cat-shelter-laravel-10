<?php

namespace App\Http\Controllers;

use App\Http\Requests\CatRequest;
use App\Http\Resources\CatResource;
use App\Models\Cat;

class CatController extends Controller
{
    public function index()
    {
        $cats = Cat::all();
        return CatResource::collection($cats);
    }

    public function store(CatRequest $request)
    {
        $cat = Cat::create($request->validated());
        return new CatResource($cat);
    }

    public function show($id)
    {
        $cat = Cat::findOrFail($id);
        return new CatResource($cat);
    }

    public function update(CatRequest $request, $id)
    {
        $cat = Cat::findOrFail($id);
        $cat->update($request->validated());
        return new CatResource($cat);
    }

    public function destroy($id)
    {
        $cat = Cat::findOrFail($id);
        $cat->delete();
        return response()->json(null, 204);
    }
}
