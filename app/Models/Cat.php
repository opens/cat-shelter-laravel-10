<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cat extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'breed',
        'chip',
        'description'
    ];

    public function branch()
    {
        return $this->belongsToMany(Branch::class);
    }

    public function user()
    {
        return $this->belongsToMany(User::class);
    }
}
