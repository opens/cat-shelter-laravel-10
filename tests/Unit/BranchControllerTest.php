<?php

namespace Tests\Unit;

use Database\Seeders\DatabaseSeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTruncation;
use Tests\TestCase;

class BranchControllerTest extends TestCase
{
    use DatabaseTruncation;
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(DatabaseSeeder::class);
    }

    public function test_index_method_returns_all_branches()
    {
        $response = $this->get('/api/branches');
        $response->assertStatus(200);
        $response->assertJsonCount(2, 'data');
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'name',
                    'address',
                    'created_at',
                    'updated_at'
                ]
            ]
        ]);
    }

    public function test_show_method_returns_specific_branch()
    {
        $branchId = 1;

        $response = $this->get("/api/branches/{$branchId}");
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'address',
                'created_at',
                'updated_at'
            ]
        ]);
    }

    public function test_store_method_creates_new_branch()
    {
        $data = [
            'name' => 'New Branch',
            'address' => '123 Main St',
        ];

        $response = $this->post('/api/branches', $data);
        $response->assertStatus(201);
        $response->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'address',
                'created_at',
                'updated_at'
            ]
        ]);
    }

    public function test_update_method_updates_existing_branch()
    {
        $branchId = 1;
        $data = [
            'name' => 'Updated Branch',
            'address' => '456 Elm St',
        ];

        $response = $this->put("/api/branches/{$branchId}", $data);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'address',
                'created_at',
                'updated_at'
            ]
        ]);
    }

    public function test_destroy_method_deletes_existing_branch()
    {
        $branchId = 1;

        $response = $this->delete("/api/branches/{$branchId}");
        $response->assertStatus(204);
    }
}
