<?php

use Database\Seeders\DatabaseSeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTruncation;
use Tests\TestCase;

class CatControllerTest extends TestCase
{
    use DatabaseTruncation;
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(DatabaseSeeder::class);
    }

    public function test_index_method_returns_all_cats()
    {
        $response = $this->get('/api/cats');
        $response->assertStatus(200);
        $response->assertJsonCount(8, 'data');
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'name',
                    'breed',
                    'chip',
                    'description',
                    'created_at',
                    'updated_at'
                ]
            ]
        ]);
    }

    public function test_show_method_returns_specific_cat()
    {
        $catId = 1;

        $response = $this->get("/api/cats/{$catId}");
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'breed',
                'chip',
                'description',
                'created_at',
                'updated_at'
            ]
        ]);
    }

    public function test_store_method_creates_new_cat()
    {
        $data = [
            'name' => 'New Cat',
            'age' => 2,
        ];

        $response = $this->post('/api/cats', $data);
        $response->assertStatus(201);
        $response->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'breed',
                'chip',
                'description',
                'created_at',
                'updated_at'
            ]
        ]);
    }

    public function test_update_method_updates_existing_cat()
    {
        $catId = 1;
        $data = [
            'name' => 'Updated Cat',
            'age' => 3,
        ];

        $response = $this->put("/api/cats/{$catId}", $data);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'breed',
                'chip',
                'description',
                'created_at',
                'updated_at'
            ]
        ]);
    }

    public function test_destroy_method_deletes_existing_cat()
    {
        $catId = 1;

        $response = $this->delete("/api/cats/{$catId}");
        $response->assertStatus(204);
    }
}
