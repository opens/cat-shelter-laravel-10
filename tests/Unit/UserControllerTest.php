<?php

use Database\Seeders\DatabaseSeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class UserControllerTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(DatabaseSeeder::class);
    }

    public function test_index_method_returns_all_users()
    {
        $response = $this->get('/api/users');
        $response->assertStatus(200);
        $response->assertJsonCount(2, 'data');
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'name',
                    'surname',
                    'email',
                    'email_verified_at',
                    'created_at',
                    'updated_at'
                ]
            ]
        ]);
    }

    public function test_show_method_returns_specific_user()
    {
        $userId = 1;

        $response = $this->get("/api/users/{$userId}");
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'surname',
                'email',
                'email_verified_at',
                'created_at',
                'updated_at'
            ]
        ]);
    }

    public function test_store_method_creates_new_user()
    {
        $data = [
            'name' => 'John',
            'surname' => 'Doe',
            'email' => 'john@example.com',
            'password' => 'secret32323',
            'password_confirmation' => 'secret32323',
        ];

        $response = $this->post('/api/users', $data);
        $response->assertStatus(201);
        $response->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'surname',
                'email',
                'email_verified_at',
                'created_at',
                'updated_at'
            ]
        ]);
    }

    public function test_update_method_updates_existing_user()
    {
        $userId = 1;
        $data = [
            'name' => 'Jane',
            'surname' => 'Smith',
            'email' => 'jane@example.com',
            'password' => 'newpassword',
            'password_confirmation' => 'newpassword',
        ];

        $response = $this->put("/api/users/{$userId}", $data);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'surname',
                'email',
                'email_verified_at',
                'created_at',
                'updated_at'
            ]
        ]);
    }

    public function test_destroy_method_deletes_existing_user()
    {
        $userId = 1;

        $response = $this->delete("/api/users/{$userId}");
        $response->assertStatus(204);
    }
}
