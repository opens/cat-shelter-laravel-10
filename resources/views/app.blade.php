<div style="margin: 0 auto; width: fit-content; font-size: 50px">
    <div>
        <a target="_blank" href="{{url('api/cats')}}">api/cats</a> |
        <a target="_blank" href="{{url('api/cats/1')}}">api/cats/1</a>
    </div>
    <hr>
    <div>
        <a target="_blank" href="{{url('api/branches')}}">api/branches</a> |
        <a target="_blank" href="{{url('api/branches/1')}}">api/branches/1</a>
    </div>
    <hr>
    <div>
        <a target="_blank" href="{{url('api/users')}}">api/users</a> |
        <a target="_blank" href="{{url('api/users/1')}}">api/users/1</a>
    </div>
    <hr>
    <div>
        <a style="font-weight: bold" target="_blank" href="https://gitlab.com/opens/cat-shelter-laravel-10">https://gitlab.com/opens/cat-shelter-laravel-10</a>
    </div>
    <div>
        Laravel Version 10.13.5
    </div>
    <hr>
    <div>
        <div> PUT|PATCH api/cats/{cat}</div>
        <div>DELETE api/cats/{cat}</div>
        <div> GET|HEAD api/users</div>
        <div>POST api/users</div>
        <div>GET|HEAD api/users/{user}</div>
        <div>PUT|PATCH api/users/{user}</div>
        <div> DELETE api/users/{user}</div>
    </div>
</div>
