<?php

namespace Database\Factories;

use App\Models\Branch;
use App\Models\Cat;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\BranchCat>
 */
class BranchCatFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'branche_id' => factory(Branch::class),
            'cat_id' => factory(Cat::class),
            'end_date' => fake()->dateTimeBetween('-1 year', '+1 year'),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
