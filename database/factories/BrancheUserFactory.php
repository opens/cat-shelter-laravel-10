<?php

namespace Database\Factories;

use App\Models\Branch;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\BranchUser>
 */
class BrancheUserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'branche_id' => factory(Branch::class),
            'user_id' => factory(User::class),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
