<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Cat>
 */
class CatFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => fake()->name,
            'breed' => fake()->word,
            'chip' => fake()->randomElement(['yes', 'no']),
            'description' => fake()->paragraph,
            'created_at' => now(),
            'updated_at' => now(),
            'photo' => null,
        ];
    }
}
