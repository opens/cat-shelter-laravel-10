<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        User::factory()
            ->has(\App\Models\Branch::factory()
                ->has(\App\Models\Cat::factory()->count(2))
                ->count(1)
            )
            ->has(\App\Models\Cat::factory()
                ->count(2))
            ->count(2)->create();
    }
}
